# -*- coding: UTF-8 -*-
'''
Author: Jaime Rivera
File: 3ddist_readonly_exec.py
Date: 2019.01.06
Revision: 2020.05.01
Copyright: Copyright 2019 Jaime Rivera

           Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
           documentation files (the "Software"), to deal in the Software without restriction, including without
           limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
           the Software, and to permit persons to whom the Software is furnished to do so, subject to the following
           conditions:

           The above copyright notice and this permission notice shall be included in all copies or substantial
           portions of the Software.

           THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
           TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
           SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
           ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
           OR OTHER DEALINGS IN THE SOFTWARE.

Brief: Executable file that lets the user calculate distances between Nuke 3D objects (cameras, geometries...)

'''

__author__ = 'Jaime Rivera <www.jaimervq.com>'
__copyright__ = 'Copyright 2019, Jaime Rivera'
__credits__ = []
__license__ = 'MIT License'
__maintainer__ = 'Jaime Rivera'
__email__ = 'jaime.rvq@gmail.com'
__status__ = 'Testing'

import nuke

# NUKE FLAGS USED
# ---------------
# STARTLINE = 0x00001000
# SLIDER = 0x00000002
# READ_ONLY = 0x10000000
# NO_ANIMATION = 0x00000100
# INVISIBLE = 0x00000400


# -------------------------------- NUMBER INPUT -------------------------------- #

total_objects = None

while total_objects is None:
    try:
        input_count = nuke.getInput("Enter number of 3D objects to compare", '')
        total_objects = int(2.0 * round(abs(float(input_count)) / 2.0))
    except ValueError:
        nuke.message('<b><font size=5 color=red>Not a valid number')
        pass

# -------------------------------- NODE CREATION -------------------------------- #

sticky_note = nuke.nodes.StickyNote(name='Distances',
                                    label='3D distances',
                                    note_font='Arial Bold',

                                    note_font_size=35,
                                    tile_color=2566914303)

# Knobs creation
for i in range(1, total_objects + 1):
    source = nuke.String_Knob('source_row_{}'.format(i), 'Source'.format(i))
    sticky_note.addKnob(source)

    button_code_source = "if len(nuke.selectedNodes())==1:" \
                         "\n    if 'translate' in nuke.selectedNode().knobs().keys():" \
                         "\n        nuke.thisNode()['source_row_{0}'].setValue(nuke.selectedNode()['name'].value())" \
                         "\n    else:" \
                         "\n        nuke.thisNode()['source_row_{0}'].setValue('')" \
                         "\nelif len(nuke.selectedNodes())>1:" \
                         "\n    nuke.message('<font size=4>Please select only one node')" \
                         "\nelse:" \
                         "\n    nuke.message('<font size=4>Please select a node')".format(i)
    source_button = nuke.PyScript_Knob('source_button_row_{}'.format(i), 'Select', button_code_source)
    source_button.setTooltip("Select a 3D object and press this button to add it to the field on the left (Source)"
                             "\nYou can also type any node's name into these fields".format(i))
    sticky_note.addKnob(source_button)

    target = nuke.String_Knob('target_row_{}'.format(i), ' Target '.format(i))
    sticky_note.addKnob(target)
    target.clearFlag(0x00001000)

    button_code_target = "if len(nuke.selectedNodes())==1:" \
                         "\n    if 'translate' in nuke.selectedNode().knobs().keys():" \
                         "\n        nuke.thisNode()['target_row_{0}'].setValue(nuke.selectedNode()['name'].value())" \
                         "\n    else:" \
                         "\n        nuke.thisNode()['target_row_{0}'].setValue('')" \
                         "\nelif len(nuke.selectedNodes())>1:" \
                         "\n    nuke.message('<font size=4>Please select only one node')" \
                         "\nelse:" \
                         "\n    nuke.message('<font size=4>Please select a node')".format(i)
    target_button = nuke.PyScript_Knob('target_button_row_{}'.format(i), 'Select', button_code_target)
    target_button.setTooltip(
        "Select a 3D object and press this button to add it to the field on the left (Target)\nYou can also type any node's name into these fields".format(
            i))
    sticky_note.addKnob(target_button)

    dist_text = nuke.Text_Knob('distance_row_{}'.format(i), '<b>&nbsp;<font color =lime>DIST: ', '000000.000')
    sticky_note.addKnob(dist_text)
    dist_text.clearFlag(0x00001000)


reset_code = "for knob in nuke.thisNode().allKnobs():"\
             "\n    if 'target_' in knob.name() or 'source_' in knob.name():" \
             "\n        if 'button' not in knob.name():" \
             "\n            knob.setValue('')"
reset_knob = nuke.PyScript_Knob('reset', '<b><font size=5>RESET ALL', reset_code)
sticky_note.addKnob(reset_knob)
reset_knob.setFlag(0x00001000)


format2_knob = nuke.Int_Knob('format2', '')
format2_knob.setExpression("[python -execlocal {for knob in nuke.thisNode().allKnobs():"
                           "\n    if 'distance_' in knob.name():"
                           "\n        import math"
                           "\n        index = knob.name()[13:]"
                           "\n        source = nuke.toNode(nuke.thisNode()['source_row_{}'.format(index)].value())"
                           "\n        target = nuke.toNode(nuke.thisNode()['target_row_{}'.format(index)].value())"
                           "\n        if source is not None and 'translate' in source.knobs().keys() and target is not None and 'translate' in target.knobs().keys():"
                           "\n            distance = math.sqrt(((source['translate'].getValue()[0] - target['translate'].getValue()[0])**2)+((source['translate'].getValue()[1] - target['translate'].getValue()[1])**2)+((source['translate'].getValue()[2] - target['translate'].getValue()[2])**2))"
                           "\n            knob.setValue('<font color =lime>'+str(round(distance,3)).zfill(10))"
                           "\n        else:"
                           "\n            knob.setValue('000000.000')"
                           "\nret=0}]")
sticky_note.addKnob(format2_knob)
format2_knob.setFlag(0x00000400)


format3_knob = nuke.Int_Knob('format3', '')
format3_knob.setExpression("[python -execlocal {for knob in nuke.thisNode().allKnobs():"
                           "\n    if 'source_row_' in knob.name() and nuke.toNode(knob.value()) is not None:"
                           "\n        if 'translate' not in nuke.toNode(knob.value()).knobs().keys():"
                           "\n            knob.setValue('')"
                           "\n    if 'target_row_' in knob.name() and nuke.toNode(knob.value()) is not None:"
                           "\n        if 'translate' not in nuke.toNode(knob.value()).knobs().keys():"
                           "\n            knob.setValue('')"
                           "\nret=0}]")
sticky_note.addKnob(format3_knob)
format3_knob.setFlag(0x00000400)


# -------------------------------- FRAMING -------------------------------- #

sticky_note.setSelected(True)
nuke.show(sticky_note)
nuke.zoomToFitSelected()
for n in nuke.allNodes():
    n.setSelected(False)
